package no.accelerate;

import no.accelerate.client.Client;
import no.accelerate.observer.newsletter.NewsletterObsClient;
import no.accelerate.observer.youtube.YoutubeObsClient;

public class Main {
    public static void main(String[] args) {
        Client opNewsLetter = new NewsletterObsClient();
        Client opYouTube = new YoutubeObsClient();
        //opNewsLetter.demonstrate();
        //opYouTube.demonstrate();
    }
}