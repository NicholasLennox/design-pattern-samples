package no.accelerate.observer.youtube;

import no.accelerate.client.Client;

/**
 * This class is responsible for using the subscribers properly.
 */
public class YoutubeObsClient implements Client {
    public void demonstrate() {
        // Make some channels
        Channel pewds = new Channel("PewDiePie");
        Channel beast = new Channel("Mr. Beast");
        Channel tseries = new Channel("T Series");
        // Subscribe
        beast.subscribe(pewds);
        beast.subscribe(tseries);
        tseries.subscribe(beast);
        tseries.subscribe(pewds);
        // Upload some videos
        beast.postNewVideo("https://www.youtube.com/watch?v=0e3GPea1Tyg");
        tseries.postNewVideo("https://www.youtube.com/watch?v=6Dh-RL__uN4");
    }
}
