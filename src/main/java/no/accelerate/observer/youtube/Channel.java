package no.accelerate.observer.youtube;

import no.accelerate.observer.abstractions.Publisher;
import no.accelerate.observer.abstractions.Subscriber;

import java.util.ArrayList;
import java.util.List;

public class Channel implements Publisher, Subscriber {

    private String channelName;
    private List<String> videos; // Links to videos

    private List<Subscriber> subscribers; // Channels subscribed to this one

    public Channel(String channelName) {
        this.channelName = channelName;
        videos = new ArrayList<>();
        subscribers = new ArrayList<>();
    }

    public void postNewVideo(String link) {
        videos.add(link);
        notifySubscribers();
    }

    @Override
    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    @Override
    public boolean subscribe(Subscriber subscriber) {
        return subscribers.add(subscriber);
    }

    @Override
    public boolean unsubscribe(Subscriber subscriber) {
        return subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers() {
        for (Subscriber sub: subscribers) {
            String latestVideo = videos.get(videos.size()-1);
            sub.update(channelName + " just posted a new video. Check it out here: " + latestVideo);
        }
    }

    @Override
    public void update(String message) {
        System.out.println(channelName + " just received a new notification: \n" + message);
    }
}
