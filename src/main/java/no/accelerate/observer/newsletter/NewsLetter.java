package no.accelerate.observer.newsletter;

import no.accelerate.observer.abstractions.Publisher;
import no.accelerate.observer.abstractions.Subscriber;

import java.util.ArrayList;
import java.util.List;

public class NewsLetter implements Publisher {
    private List<Subscriber> subscribers;
    private String message; // This is our state

    public NewsLetter() {
        subscribers = new ArrayList<>();
    }

    public void updateNewsLetter(String message) {
        this.message = message;
        notifySubscribers();
    }


    @Override
    public List<Subscriber> getSubscribers() {
        return subscribers;
    }

    @Override
    public boolean subscribe(Subscriber subscriber) {
        return subscribers.add(subscriber);
    }

    @Override
    public boolean unsubscribe(Subscriber subscriber) {
        return subscribers.remove(subscriber);
    }

    @Override
    public void notifySubscribers() {
        for (Subscriber sub: subscribers) {
            sub.update(message);
        }
    }
}
