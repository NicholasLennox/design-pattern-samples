package no.accelerate.observer.newsletter;

import no.accelerate.client.Client;

/**
 * This class is responsible for using the subscribers properly.
 */
public class NewsletterObsClient implements Client {

    public void demonstrate() {
        // Set up some people
        Person per1 = new Person("John");
        Person per2 = new Person("Jane");
        Person per3 = new Person("Jon");
        Person per4 = new Person("Joan");
        // Setup some newsletters
        NewsLetter importantNewsDotCom = new NewsLetter();
        NewsLetter notSoImportantNewsDotCom = new NewsLetter();
        // Subscribe
        importantNewsDotCom.subscribe(per1);
        importantNewsDotCom.subscribe(per2);
        importantNewsDotCom.subscribe(per3);
        notSoImportantNewsDotCom.subscribe(per1);
        notSoImportantNewsDotCom.subscribe(per3);
        notSoImportantNewsDotCom.subscribe(per4);
        // Publish some newsletters
        importantNewsDotCom.updateNewsLetter("Hello world! Its our first post.");
        importantNewsDotCom.updateNewsLetter("Spam spam jam jam.");
        notSoImportantNewsDotCom.updateNewsLetter("I dont know why you all even subscribed?");
        // Unsubscribe
        notSoImportantNewsDotCom.unsubscribe(per1);
        notSoImportantNewsDotCom.unsubscribe(per4);
        // Another post
        notSoImportantNewsDotCom.updateNewsLetter("We still have subscribers? AMAZING.");
    }
}
