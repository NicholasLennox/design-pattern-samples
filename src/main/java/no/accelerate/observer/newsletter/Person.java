package no.accelerate.observer.newsletter;

import no.accelerate.observer.abstractions.Subscriber;

import java.util.ArrayList;
import java.util.List;

public class Person implements Subscriber {
    List<String> newsletters;
    String name;

    public Person(String name) {
        newsletters = new ArrayList<>();
        this.name = name;
    }

    @Override
    public void update(String message) {
        // When we get a new newsletter, we just add it to our list.
        newsletters.add(message);
        // Print a message to say they get the message
        System.out.println("My name is " + name +
                " and I just got the following newsletter: \n" + message);
    }
}
