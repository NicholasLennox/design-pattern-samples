package no.accelerate.observer.abstractions;

/**
 * Provides the behaviour of a class that can subscribe to another class.
 * This allows the subscriber to "watch" for changes and be notified via the update method.
 * The publisher and subscriber don't need to be different classes. It can be the same class
 * if your business logic requires it.
 *
 * For example: YouTube, channels are both publishers and subscribers.
 */
public interface Subscriber {
    void update(String message);
}
