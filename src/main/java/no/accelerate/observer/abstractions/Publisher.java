package no.accelerate.observer.abstractions;

import java.util.List;

/**
 * Assigns the behaviour of a publisher.
 * This is a class that is observed by subscribers and notifies them when state changes.
 *
 * Once again, this can be the same concrete class as a subscriber.
 * YouTube is once again used as an example.
 */
public interface Publisher {
    /**
     * @return Collection of subscribers for the publisher.
     */
    List<Subscriber> getSubscribers();

    /**
     * Adds the subscriber to the list of subscribers for the publisher.
     * @param subscriber
     * @return if the subscription was a success
     */
    boolean subscribe(Subscriber subscriber);

    /**
     * Removes the subscriber from the list in the publisher.
     * @param subscriber
     * @return if the unsubscription was a success
     */
    boolean unsubscribe(Subscriber subscriber);

    /**
     * Calls the subscriber.update(state) method for all subscribers.
     */
    void notifySubscribers();
}
